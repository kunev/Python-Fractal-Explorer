import math
import tokens
token = tokens.token

ops_dictionary = {'-':  lambda x,y: x-y,
                  '+':  lambda x,y: x+y,
                  '/':  lambda x,y: x/y,
                  '*':  lambda x,y: x*y,
                  '**': lambda x,y: x**y,}
def extend_math_func(func):
    def rslt(arg):
        if type(arg) == complex:
            return func(arg)
        else:
            return math.__getattribute__(func.__name__)(arg)

    return rslt

@extend_math_func
def sin(z):
    return math.sin(z.real)*math.cosh(z.imag) + 1j*math.cos(z.real)*math.sinh(z.imag)

@extend_math_func
def cos(z):
    return math.cos(z.real)*math.cosh(z.imag) - 1j*math.sin(z.real)*math.sinh(z.imag)

@extend_math_func
def exp(z):
    return math.exp(z.real)*(math.cos(z.imag) + 1j*math.sin(z.imag))

@extend_math_func
def cosh(z):
    return math.cosh(z.real)*math.cos(z.imag) + 1j*math.sinh(z.real)*math.sin(z.imag)

@extend_math_func
def sinh(z):
    return math.sinh(z.real)*math.cos(z.imag) + 1j*math.cosh(z.real)*math.sin(z.imag)

@extend_math_func
def tan(z):
    return sin(z)/cos(z)

@extend_math_func
def tanh(z):
    return sinh(z)/cosh(z)
        

functions = {name:math.__getattribute__(name) for name in tokens.func_names if name != 'abs'}
functions['abs'] = abs
functions['re'] = lambda c: c.real
functions['im'] = lambda c: c.imag
functions['sin'], functions['cos'], functions['exp'] = sin, cos, exp
functions['cosh'], functions['sinh'], functions['tan'], functions['tanh'] = cosh, sinh, tan, tanh

class expression_tree:
    def __init__(self, tkn, left=None, right=None):
        self.token = tkn
        self.left, self.right = left, right

    def __call__(self, **kwargs):
        if self.token.type == token.NUM:
            if self.token.is_complex():
                return complex(self.token)
            return float(self.token)

        if self.token.type == token.VAR:
            if self.token.str not in kwargs:
                raise tokens.ExpressionError('Expression evaluation needs a value for the variable {0}'.format(self.token.str))
            return kwargs[self.token.str]

        if self.token.type == token.FUNC:
            return functions[self.token.str](self.left(**kwargs))

        if self.token.type == token.OP:
            return ops_dictionary[self.token.str](self.left(**kwargs), self.right(**kwargs))

    def __repr__(self):
        return '<expression_tree: {0},\nleft: {1},\nright:{2}>'.format(self.token, self.left, self.right)

    def __eq__(self, rhs):
        if not isinstance(rhs, expression_tree): return False
        return self.token == rhs.token and self.left == rhs.left and self.right == rhs.right

    def _operation(self, rhs, op):
        if not isinstance(rhs, expression_tree): raise TypeError('Only expression_tree objects can be given as operands to {0}'.format(op))

        return expression_tree(tkn=token(op), left=self, right=rhs)

    def __add__(self, rhs): return self._operation(rhs, '+')
    def __sub__(self, rhs): return self._operation(rhs, '-')
    def __mul__(self, rhs): return self._operation(rhs, '*')
    def __truediv__(self, rhs): return self._operation(rhs, '/')
    def __pow__(self, rhs): return self._operation(rhs, '**')

    def _roperation(self, rhs, op):
        if not isinstance(rhs, expression_tree): raise TypeError('Only expression_tree objects can be given as operands to {0}'.format(op))

        return expression_tree(tkn=token(op), left=rhs, right=self)

    def __radd__(self, rhs): return self._roperation(rhs, '+')
    def __rsub__(self, rhs): return self._roperation(rhs, '-')
    def __rmul__(self, rhs): return self._roperation(rhs, '*')
    def __rtruediv__(self, rhs): return self._roperation(rhs, '/')
    def __rpow__(self, rhs): return self._roperation(rhs, '**')

    def get_curry(self, **kwargs):
        if self.token.type == token.VAR and self.token.str in kwargs:
            if type(kwargs[self.token.str]) == complex:
                re, im = kwargs[self.token.str].real, kwargs[self.token.str].imag
                rslt = expression_tree(token('+'))
                if re < 0: left = expression_tree(token('-'), left = expression_tree(token('0')), right = expression_tree(token(str(abs(re)))))
                else: left = expression_tree(token(str(re)))

                if im < 0: right = expression_tree(token('-'), left = expression_tree(token('0')),right = expression_tree(token(str(abs(im))+'j')))
                else: right = expression_tree(token(str(im)+'j'))

                rslt = expression_tree(token('+'), left=left, right=right)
            else:
                rslt = expression_tree(token(str(kwargs[self.token.str])))
        else:
            rslt = expression_tree(self.token)

            rslt.left = self.left.get_curry(**kwargs) if self.left else None
            rslt.right = self.right.get_curry(**kwargs) if self.right else None

        return rslt

    def __getattr__(self, attr):
        if attr == 'var_names':
            if self.token.type == token.VAR:
                rslt = {self.token.str}
            else:
                rslt = set()

            if self.left:
                rslt.update(self.left.var_names)
            if self.right:
                rslt.update(self.right.var_names)

            return rslt
        else:
            return object.__getattribute__(self, attr)
