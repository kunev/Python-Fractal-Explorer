#!/usr/bin/python3
import pygame
import complex_iteration
import evaluation
from fractal_drawer import fractal_drawer
import drawer_serialization
import sys
import ui
import input

DrawerSerializationError = drawer_serialization.DrawerSerializationError

expression = evaluation.expression

iterate = complex_iteration.iterate_complex
mandel_func = lambda *, z, c: z**2+c
get_julia = lambda *, c: lambda *, z: z**2+c

expr = None

class QUIT(Exception):pass

if __name__ == '__main__':
    try:
        pygame.init()
        if len(sys.argv) > 1:
            file_name = sys.argv[1]
            print('Loading state from {0}'.format(file_name))
            try:
                drawer, size = drawer_serialization.load_from_file(file_name)
            except IOError:
                print('That filename won\'t do... Bye')
                exit()

            drawer.screen = pygame.display.set_mode(size)
            drawer.zoom_back()
            pygame.display.flip()

        else:
            width,height = input.input_size()
            plane = complex_iteration.complex_plane(*input.input_plane())

            colorizer = input.choose_colorizer()
            fractal_type = input.input_fractal_type()
            iters = input.input_iterations()
            bailout = input.input_bailout_value()

            drawer_args = {'plane'          : plane,
                           'colorizer'      : colorizer,
                           'max_iters'      : iters,
                           'start_iter'     : 0,
                           'bailout_val'    : bailout,
                          }

            if fractal_type == 'm':
                expr = expression('z**2+c')
                expr.mandel = None
                drawer_args.update({'func'        : mandel_func,
                                    'iter_var'    : 'z',
                                    'num_var'     : 'c',
                                    'start_iter'  : 0,
                                   })

            elif fractal_type == 'j':
                c = input.input_julia_constant()
                julia_func = get_julia(c = c)
                expr = expression('z**2+{0}'.format(c))
                expr.julia = None
                drawer_args.update({'func'         : julia_func,
                                    'iter_var'     : 'z',
                                    'num_var'      : 'z',
                                    'start_iter'   : 0,
                                   })


            elif fractal_type == 'o':
                fract_args, expr = input.input_other()
                drawer_args.update(fract_args)


            screen = pygame.display.set_mode((width, height))
            pygame.display.set_caption('Python Fractal Explorer')
            drawer_args['screen'] = screen
            drawer = fractal_drawer(**drawer_args)
            drawer.draw()

        try:
            while True:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        raise QUIT
                    elif event.type == pygame.MOUSEBUTTONDOWN:
                        ui.mouse_button_handler(event, drawer)
                    elif event.type == pygame.KEYDOWN:
                        ui.keyboard_press_handler(event, drawer, expr)
        except QUIT:
            print()
            print('Exiting...')
    except KeyboardInterrupt:
        print('\nKBYETHX')
