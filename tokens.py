import re
import collections

parentheses = ['(', ')']
func_names = ['sinh', 'cosh','sin', 'cos', 'tanh', 'tan', 'asin', 'acos', 'atan', 'exp', 'abs', 'sqrt', 'log']
op_dict = ['+', '-', '/', '*', '**']
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

reg_ex_func = r'(?:sinh)|(?:cosh)|(?:tanh)|(?:sin)|(?:cos)|(?:tan)|(?:asin)|(?:acos)|(?:atan)|(?:exp)|(?:abs)|(?:sqrt)|(?:re)|(?:im)|(?:log)'
reg_ex_number = r'\d+(?:\.\d+)?(e[-+]\d+)?(?:j)?'
reg_ex_var = r'[a-zA-Z]'
reg_ex_op = r'\+|-|\*{2}|\*|/'
reg_ex_paren = r'\(|\)'
reg_ex_comma = r','

expression_strings = [reg_ex_func, reg_ex_number, reg_ex_var, reg_ex_op, reg_ex_paren, reg_ex_comma]
expressions = [re.compile(reg_ex) for reg_ex in expression_strings]


class ExpressionError(Exception):pass

class TokenizeException(Exception):pass
class BadTokenValue(TokenizeException):pass
class TokenException(TokenizeException):pass
class TokenConvertException(TokenException):pass

class token:
    """Represents an expression token"""
    NUM = 'num'
    VAR = 'var'
    PAREN = 'paren'
    OP = 'op'
    FUNC = 'func'
    COMMA = 'comma'

    ASSOC_LEFT = 'left'
    ASSOC_RIGHT = 'right'

    operator_precendes = {'-':0, '+':0, '/':1, '*':1, '**':2}

    token_reg_exs = collections.OrderedDict({re.compile(reg_ex_func + '$'):                     FUNC,
                                           re.compile(reg_ex_number + '$'):                     NUM,
                                           re.compile(reg_ex_var + '$'):                        VAR,
                                           re.compile('(?:{0}){1}'.format(reg_ex_op, '$')):     OP,
                                           re.compile(reg_ex_paren + '$'):                      PAREN,
                                           re.compile(reg_ex_comma + '$'):                      COMMA})

    left_assoc = ['+', '-', '*', '/']
    right_assoc = ['**']

    def __init__(self, string):
        self.str = string
        self.__eval_type()

    def __eval_type(self):
        for reg_ex in token.token_reg_exs:
            if reg_ex.match(self.str):
                self.__dict__['type'] = token.token_reg_exs[reg_ex]
                if self.type == token.OP:
                    if self.str in token.left_assoc:
                        self.assoc = token.ASSOC_LEFT
                    else:
                        self.assoc = token.ASSOC_RIGHT
                    
                return

        raise BadTokenValue('{0} is not a valid token'.format(self.str))

    def is_complex(self):
        return self.str[-1] == 'j'

    def __setattr__(self, attr_name, val):
        if attr_name == 'type':
            raise TokenException('You can not change the type of a token!')
        else:
            object.__setattr__(self, attr_name, val)

    def __str__(self):
        return '<{0} token: {1}>'.format(self.type, self.str)

    def __len__(self):
        return len(self.str)

    def __eq__(self, rhs):
        """t = token(a)
            t == a -> True
            t == int(a) -> False
            t == float(a) -> False
            t == b <-> a == b"""
        return self.str == rhs

    def __int__(self):
        if not self.type == token.NUM:
            raise TokenConvertException('Can not cast token of type {0} to integer'.format(self.type))

        return int(self.str)

    def __float__(self):
        if not self.type == token.NUM:
            raise TokenConvertException('Can not cast token of type {0} to float'.format(self.type))

        return float(self.str)

    def __complex__(self):
        if not self.type == token.NUM:
            raise TokenConvertException('Can not cast token of type {0} to complex'.format(self.type))
        return complex(self.str)

    __repr__ = __str__


    def __lt__(self, rhs):
        if self.type != token.OP or rhs.type != token.OP:
            raise TokenException('< and > only work with OP tokens to determine operation prioroty')

        return token.operator_precendes[self.str] < token.operator_precendes[rhs.str]

    def __le__(self, rhs):
        if self.type != token.OP or rhs.type != token.OP:
            raise TokenException('< and > only work with OP tokens to determine operation prioroty')

        return token.operator_precendes[self.str] <= token.operator_precendes[rhs.str]
