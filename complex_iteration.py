class complex_plane:
    def __init__(self, upper_left, lower_right):
        self.upper_left, self.lower_right = upper_left, lower_right

    def __getattr__(self, attr):
        if attr == 'width':
            return self.lower_right.real - self.upper_left.real
        elif attr == 'height':
            return self.upper_left.imag - self.lower_right.imag
        elif attr == 'center':
            return (self.upper_left+self.lower_right)/2
        else:
            return object.__getattribute__(self, attr)

    def get_pixel_width(self, screen):
        return self.width/screen.get_width()

    def get_pixel_height(self, screen):
        return self.height/screen.get_height()

    def __setattr__(self, attr, val):
        if attr in ['upper_left', 'lower_right'] and type(val) != complex:
            raise ValueError('Complex plane corners must be complex numbers given a {0} object instead'.format(type(val)))
        else:
            object.__setattr__(self, attr, val)
 
    def pixel_to_complex(self, screen, coords):
        """Converts the given coordinates of a pixel in the specified screen to
           a corresponding complex number in the plane."""
        x,y = coords
        pixel_width, pixel_height = self.get_pixel_width(screen), self.get_pixel_height(screen)
#        x += pixel_width
#        y += pixel_height

        real = self.upper_left.real + (self.width/screen.get_width())*x
        imag = self.upper_left.imag - (self.height/screen.get_height())*y
        return complex(real,imag)

    def complex_to_pixel(self, screen, number):
        """Returns the coordinates of the pixel on the given screen,
           corresponding to the given complex number"""
        x = round((number.real - self.upper_left.real) * (screen.get_width() / self.width))
        y = round((self.upper_left.imag - number.imag) * (screen.get_height() / self.height))
        return x,y

    def __str__(self):
        return '<complex_plane: {0}; {1}>'.format(self.upper_left, self.lower_right)

def iterate_complex(*args, func, max_iterations, bailout, arguments_dict, iter_var):
    iterations = 0
    while iterations < max_iterations and not bailout(arguments_dict[iter_var]):
        arguments_dict[iter_var] = func(*args, **arguments_dict)
        iterations += 1

    return iterations,arguments_dict[iter_var]
