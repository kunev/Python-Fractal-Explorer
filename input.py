from colorizers import colorizers
from evaluation import expression

def is_num(arg):
    try:
        complex(arg)
        return True
    except TypeError:
        return False
    except ValueError:
        return False

def input_size():
    while True:
        width_height = input('Choose desired screen resolution(width and height separated by a space): ').split(' ')
        try:
            width, height = (int(num) for num in width_height)
            if width*height == 0:
                raise ValueError
            return width, height
        except ValueError:
            print('Those weren\'t two numbers that could be used for screen resolution... I\'ll give you another try')

def input_plane():
    while True:
        ul_lr = []
        while len(ul_lr) != 2:
            ul_lr = input('Input the upper-left-most and lower-right-most complex number of the complex plane you would like to draw: ').split(' ')
        try:
            ul, lr = (complex(num) for num in ul_lr)
            if (ul-lr).real == 0 or (ul-lr).imag == 0:
                raise ValueError
            return ul, lr
        except ValueError:
            print('Please use valid Python complex number notation(e.g. 5.3+2.1j) and try to use numbers that would make sense.')

def input_iterations():
    while True:
        try:
            iters = int(input('Maximum iterations per pixel: '))
            if iters <= 0:
                raise ValueError
            return iters
        except ValueError:
            print('The number of iterations should be a valid positive integer')

def input_other():
    while True:
        try:
            formula_str = input('Formula to iterate: ')
            expr = expression(formula_str)
            break
        except:
            print('Something isn\'t right with that formula, please try again.')

    fract_expr = expr.get_callable()

    drawer_args = {'func':fract_expr}

    while True:
        number_var = input('Which is the variable that should be assigned the current number when iterating: ')
        if not (len(number_var) == 1 and number_var.isalpha()):
            print('A variable name is just one letter. Don\'t worry... Here, try again...')
            continue
        if not number_var in fract_expr.var_names:
            print('{0} is not a variable of the given formula. Think hard and try again.')
            continue

        drawer_args['num_var'] = number_var
        break

    while True:
        iter_var = input('Which is the iteration variable(if you want to iterate z=f(z), than z is your iteration variable): ')
        if not (len(iter_var) == 1 and iter_var.isalpha()):
            print('A variable name is just one letter. Don\'t worry... Here, try again...')
            continue
        if not iter_var in fract_expr.var_names:
            print('{0} is not a variable of the given formula. Think hard and try again.')
            continue

        drawer_args['iter_var'] = iter_var
        break

    if not iter_var == number_var:
        while True:
            start_iter = input('What should be the initial value for {0}: '.format(iter_var))
            if not is_num(start_iter):
                print('This is not a number... Come on.. FOCUS!!!')
                continue

            drawer_args['start_iter'] = complex(start_iter)
            break
    else:
        drawer_args['start_iter'] = None
 
    return drawer_args, expr

def input_fractal_type():
    rslt = ''
    while rslt not in {'m', 'j', 'o'}:
        try:
            rslt = input('Draw (M)andelbrot set, Quadratic (J)ulia set or (O)ther : ').lower()[0]
        except IndexError:
            print('Seriouzly... at least type in something... ')

    return rslt

def choose_colorizer():
    print('Please choose one of the following colorizers:')
    for num,colorizer in enumerate(colorizers):
        print('{0}) {1}'.format(num, colorizer.__name__))

    while True:
        try:
            colorizer_num = int(input())
            if colorizer_num not in range(len(colorizers)): raise ValueError
            return colorizers[colorizer_num]
        except ValueError:
            print('Come on... a simpe little integer between 0 and {0}... FOCUS!'.format(len(colorizers)-1))

def input_zoom_coefficient():
    while True:
        try:
            rslt = float(input('New zoom coefficient(integer) : '))
            print('Zoom coeficient set to {0}'.format(rslt))
            return rslt
        except ValueError:
            print('It\'s quite simple really... choose a floating point number and write it in here. :)')

def input_julia_constant():
    while True:
        try:
            julia_c = input('Complex constant for the julia fractal: ')
            return complex(julia_c)
        except ValueError:
            print('The complex constant must be a valid complex number(ORLY?!)')

def input_save_file_name():
    name = ''
    while name == '':
        name = input('File name to save to: ')

    if not name.endswith('.pfe'):
        name += '.pfe'

    return name

def input_image_file_name():
    name = ''
    while name == '':
        name = input('File name to save image to: ')

    return name

def input_bailout_value():
    while True:
        try:
            bailout = float(input('Baliout value for iteration : '))
            return bailout
        except ValueError:
            print('Nope... this one should be some kind of number... try a real one... trust me :)')

def input_animation_file_names():
    name = ''
    while name == '':
        name = input('Frame file names: ')

    return name

