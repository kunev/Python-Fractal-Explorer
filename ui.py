import pygame
import input
import drawer_serialization

DrawerSerializationError = drawer_serialization.DrawerSerializationError

def mouse_button_handler(event, drawer):
    center = drawer.plane.pixel_to_complex(drawer.screen, event.pos)

    #Left mouse button
    if event.button == 1:
        drawer.zoom_in_to_pixel(event.pos)
    #Mddle mouse button
    elif event.button == 2:
        print('Redrawing same.')
        drawer.draw()
        return

    #Right mouse button
    elif event.button == 3:
        drawer.zoom_out_from_pixel(event.pos)
    #Mouse wheel up
    elif event.button == 4:
        drawer.max_iterations += 10
        print('Rose iterations per pixel by 10 to {0}'.format(drawer.max_iterations))
        return

    #Mouse wheel down
    elif event.button == 5:
        drawer.max_iterations -= 10
        print('Lowered iterations per pixel by 10 to {0}'.format(drawer.max_iterations))
        return

    print('Starting to draw {0}'.format(drawer.plane))
    drawer.draw()

#TODO Keyboard interaction
def keyboard_press_handler(event, drawer, expr):
    #r - redraws the current frame
    if event.key == pygame.K_r:
        print('Redrawing same.')
        drawer.draw()
        return
    #plus - raises iterations by 20
    elif event.key in [pygame.K_PLUS, pygame.K_KP_PLUS]:
        drawer.max_iterations += 10
        print('Rose iterations per pixel by 10 to {0}'.format(drawer.max_iterations))
        return
    #minus - lowers iterations by 20
    elif event.key in [pygame.K_MINUS, pygame.K_KP_MINUS]:
        drawer.max_iterations -= 10
        print('Lowered iterations per pixel by 10 to {0}'.format(drawer.max_iterations))
        return
    #i - zooms in to the center of the screen
    elif event.key == pygame.K_i:
        drawer.zoom_in_to_number(drawer.center)
        print('Zooming in to center.')
        drawer.draw()
    #o - zooms out from the center
    elif event.key == pygame.K_o:
        drawer.zoom_out_from_number(drawer.center)
        drawer.draw()
    #c - choose colorizer
    elif event.key == pygame.K_c:
        drawer.colorizer = input.choose_colorizer()
    #z - set zoom coefficient
    elif event.key == pygame.K_z:
        drawer.zoom_coef = input.input_zoom_coefficient()

    #b - zoom out back to the previous screen without calculating it again
    elif event.key == pygame.K_b:
        drawer.zoom_back()
        print('Zooming out back to {0}'.format(drawer.plane))
        pygame.display.flip()

    #a - change bailout value
    elif event.key == pygame.K_a:
        drawer.bailout_val = input.input_bailout_value()

    #s - save current state of the drawer to file
    #Still not quite though :)
    elif event.key == pygame.K_s:
        name = input.input_save_file_name()
        print('Saving state for {0} to {1}'.format(expr.string, name))
        while True:
            try:
                drawer_serialization.save_to_file(name, drawer, expr)
                return
            except DrawerSerializationError as exc:
                print(exc)
                name = input.input_save_file_name()

        print('Succesfully saved file {0}'.format(name))

    #e - export bmp/png/tga/jpeg file
    elif event.key == pygame.K_e:
        name = input.input_image_file_name()
        pygame.image.save(drawer.screen, name)
        print('Saved image successfully')

    #x - export entire images_stack to files
    elif event.key == pygame.K_x:
        name = input.input_animation_file_names()
        drawer.export_stack_to_images(name)
        print('Exported stack succeccfully')
