import math
import re
import collections
from tokens import *
from expr_tree import expression_tree

class TokenizeError(Exception):pass

class expression:
    """Class to hold expressions both in infix and postfix notation and create
       callables , which calculate the result of the given string as a formula"""
    def __init__(self, string):
        self.postfix = None
        self.string = string.replace(' ' , '').lower()
        self.tokenize()

    def __len__(self):
        return len(self.tokens)

    def tokenize(self):
        tokens = []
        string = self.string[:]
        while string:
            matches = [reg_ex.match(string) for reg_ex in expressions]
            if len(matches) == 0:
                raise TokenizeError('Can not tokenize \'{0}\''.format(self.string))
            for match in matches:
                if match is not None:
                    tokens.append(token(match.group()))
                    string = string[len(tokens[-1]):]
                    break

        self.tokens = tokens

    def generate_postfix(self):
        output_queue, stack = [], []
        for tkn in self.tokens:
            #If number or variable enqueue
            if tkn.type in (token.NUM, token.VAR):
                output_queue.append(tkn)

            #If function push to stack
            elif tkn.type == token.FUNC:
                stack.append(tkn)

            #If comma try to pop out untill parren
            elif tkn.type == token.COMMA:
                try:
                    while stack[-1].str != '(':
                        output_queue.append(stack.pop())
                    else:
                        stack.pop()
                except IndexError as err:
                    raise ExpressionError('Misplaced comma or mismatched paren in {0}'.format(self), *err.args)

            #If operation pop out preceding operations from stack to queue and push current token in stack
            elif tkn.type == token.OP:
                try:
                    while stack[-1].type == token.OP:
                        if tkn.assoc == token.ASSOC_LEFT and tkn <= stack[-1] or tkn < stack[-1]:
                            output_queue.append(stack.pop())
                        else:
                            break
                except IndexError: pass
                stack.append(tkn)

            #If left parren push to stack
            elif tkn.str == '(':
                stack.append(tkn)

            #If right paren pop arguments untill left paren
            elif tkn.str == ')':
                try:
                    while stack[-1].str != '(':
                        output_queue.append(stack.pop())
                    stack.pop()
                except IndexError as err:
                    raise ExpressionError('Missmatched parens in {0}'.format(self))

                try:
                    if stack[-1].type == token.FUNC:
                        output_queue.append(stack.pop())
                except:
                    pass

        #If something's left on the tack pop it. If it's a left paren something's wrong
        while len(stack) > 0:
            if stack[-1].str == '(':
                raise ExpressionError('Missmatched parens in {0}'.format(self))
            output_queue.append(stack.pop())
        self.postfix = output_queue

    def get_callable(self):
        if self.postfix == None:
            self.generate_postfix()

        nodes_stack = []
        nodes = [expression_tree(tkn) for tkn in self.postfix]
        for tkn_node in nodes:
            if tkn_node.token.type == token.VAR or tkn_node.token.type == token.NUM:
                nodes_stack.append(tkn_node)
            elif tkn_node.token.type == token.OP:
                if tkn_node.token.str in ['-', '+'] and len(nodes_stack) < 2:
                    nodes_stack.insert(0, expression_tree(token('0')))
                tkn_node.right, tkn_node.left = nodes_stack.pop(), nodes_stack.pop()
                nodes_stack.append(tkn_node)
            elif tkn_node.token.type == token.FUNC:
                tkn_node.left = nodes_stack.pop()
                nodes_stack.append(tkn_node)

        nodes_stack[0].string = self.string

        return nodes_stack[0]

    def __str__(self):
        return self.string
    def __repr__(self):
        return '<expression object: {0}>'.format(self.string)
