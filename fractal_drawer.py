from complex_iteration import iterate_complex
from pygame import PixelArray, display, image,  Rect
from colorizers import colorizers
import pickle
import os.path

class fractal_drawer:
    def __init__(self, *, func, screen, plane, iter_var, num_var, start_iter, max_iters, bailout_val, colorizer):
        self.iter_args = {'func'                : func,
                          'max_iterations'      : max_iters,
                          'bailout'             : lambda z:abs(z)>=bailout_val,
                          'arguments_dict'      : {iter_var: start_iter},
                          'iter_var'            : iter_var,
                         }
        self.plane, self.screen = plane, screen
        self.colorizer = colorizer
        self.num_var, self.start_iter = num_var, start_iter
        self.zoom_coef = 0.5

        self.images_stack = []
        self.bailout_val = bailout_val

    def __setattr__(self, attr, val):
        if attr == 'bailout_val':
            object.__setattr__(self, attr, val)
            self.bailout = lambda z:abs(z)>=val
        if attr in ['iter_args', 'plane']:
            object.__setattr__(self, attr, val)
        elif attr in self.iter_args:
            self.iter_args[attr] = val
        elif hasattr(self.plane, attr):
            setattr(self.plane, attr, val)
        else:
            object.__setattr__(self, attr, val)

    def __getattr__(self, attr):
        if attr in ['iter_args', 'plane']:
            return object.__getattribute__(self, attr)
        elif attr in self.iter_args:
            return self.iter_args[attr]
        elif hasattr(self.plane, attr):
            return getattr(self.plane, attr)
        else:
            return object.__getattribute__(self, attr)

    def _pixel_to_complex(self, coords):
        return self.plane.pixel_to_complex(self.screen, coords)

    def zoom_in_to_number(self, center_number):
        self._push_image_to_stack(self.screen)
        self.upper_left , self.lower_right =\
                        center_number - self.width*self.zoom_coef/2 + 1j*self.height*self.zoom_coef/2,\
                        center_number + self.width*self.zoom_coef/2 - 1j*self.height*self.zoom_coef/2

    def zoom_out_from_number(self, center_number):
        self._push_image_to_stack(self.screen)
        self.upper_left , self.lower_right =\
                        center_number - self.width/(self.zoom_coef*2) + 1j*self.height/(self.zoom_coef*2),\
                        center_number + self.width/(self.zoom_coef*2) - 1j*self.height/(self.zoom_coef*2)

    def zoom_in_to_pixel(self, center_pixel):
        self.zoom_in_to_number(self._pixel_to_complex(center_pixel))

    def zoom_out_from_pixel(self, center_pixel):
        self.zoom_out_from_number(self._pixel_to_complex(center_pixel))

    def zoom_back(self):
        self._pop_image_from_stack()

    def iterate_pixel(self, coords):
        number = self._pixel_to_complex(coords)

        self.arguments_dict[self.iter_var] = self.start_iter
        self.arguments_dict[self.num_var] = number

        return iterate_complex(**self.iter_args)

    def draw(self):
        width, height = self.screen.get_width(), self.screen.get_height()
        percent = 0
        try:
            for x in range(width):
                for y in range(height):
                    iterations, final_val = self.iterate_pixel((x, y))
    
                    self.screen.lock()
                    self.screen.set_at((x, y), self.colorizer(iterations, final_val, self.iter_args['max_iterations']))
                    self.screen.unlock()

#                   display.update(Rect(x,y,1,1))

                new_percent = int(x/width*100)
                if new_percent > percent:
                    percent = new_percent
                    print('\r{0}% complete'.format(percent), end = '')
        except KeyboardInterrupt:
            print('\nCanceled drawing at {0}%. Press r or middle click in to redraw.'.format(percent))

        display.flip()
        print('\nDone!')

    def _get_array_from_surface(self, surface):
        col_arr = []
        for x in range(surface.get_width()):
            col_arr.append([])
            for y in range(surface.get_height()):
                col_arr[x].append(self.screen.get_at((x,y)))

        return col_arr

    def _load_array_to_surface(self, arr):
        for x in range(self.screen.get_width()):
            for y in range(self.screen.get_height()):
                self.screen.set_at((x, y), arr[x][y])

    def _push_image_to_stack(self, screen):
        self.images_stack.append((image.tostring(self.screen, 'RGB'), self.upper_left, self.lower_right))

    def _pop_image_from_stack(self):
        try:
            surface_string, self.upper_left, self.lower_right = self.images_stack.pop()
            popped_surface = image.fromstring(surface_string, (self.screen.get_width(), self.screen.get_height()), 'RGB')
            self.screen.blit(popped_surface, (0,0))
        except IndexError: pass

    def export_stack_to_images(self, filename):
        for num, img in enumerate(self.images_stack):
            name = filename + str(num) + '.bmp'
            surface = image.fromstring(img[0], (self.screen.get_width(), self.screen.get_height()), 'RGB')
            image.save(surface, name)
            
