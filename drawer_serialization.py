from fractal_drawer import fractal_drawer
from evaluation import expression
import pickle, os
from colorizers import colorizers

class DrawerSerializationError(Exception):pass

mandel_func = lambda *, z, c: z**2+c
get_julia = lambda *, c: lambda *, z: z**2+c

def save_to_file(file_name, drawer, expr):
    if os.path.exists(file_name) and os.path.isfile(file_name):
        raise DrawerSerializationError('File {0} exists.'.format(file_name))

    inbuilt_fractal = {'mandel'   : hasattr(expr, 'mandel'),
                       'julia'    : hasattr(expr, 'julia'),
                      }
    with open(file_name, 'wb') as file:
        if inbuilt_fractal['mandel'] and inbuilt_fractal['julia']:
            raise DrawerSerializationError('Expression can be mandelbrot, julia or neither, but not both')

        pickle.dump(inbuilt_fractal, file)             #Pickle flags if the fractal is a mandelbrot or julia set
        pickle.dump(expr.string, file)                 #Pickle the expression string

        pickle.dump(drawer.zoom_coef, file)
        drawer._push_image_to_stack(drawer.screen)
        pickle.dump(drawer.images_stack, file)         #Pickle the image stack
        pickle.dump(drawer.plane, file)                #Pickle the complex plane of the drawer

        pickle.dump(drawer.screen.get_width(), file)
        pickle.dump(drawer.screen.get_height(), file)  #Pickle the screen size

        pickle.dump(drawer.max_iterations, file)       #Pickle the maximum iteration count of the drawer
        pickle.dump(drawer.iter_var, file)             #Pickle the iteration variable of the drawer
        pickle.dump(drawer.num_var, file)              #
        pickle.dump(drawer.start_iter, file)           #
        pickle.dump(drawer.bailout_val, file)          #

        pickle.dump(drawer.colorizer.__name__, file)   #

    print('Successfully saved current state to {0}'.format(file_name))

def load_from_file(file_name):
    with open(file_name, 'rb') as file:
        inbuilt_fractal = pickle.load(file)
        expr_string = pickle.load(file)
        if inbuilt_fractal['mandel']:
            func = mandel_func
        elif inbuilt_fractal['julia']:
            #FUGLY
            c = complex(expr_string.split('2+')[1])
            func = get_julia(c = c)
        else:
            func = expression(expr_string)

        zoom_coef = pickle.load(file)

        stack = pickle.load(file)
        plane = pickle.load(file)
        w, h = pickle.load(file), pickle.load(file)

        max_iterations = pickle.load(file)
        iter_var = pickle.load(file)
        num_var = pickle.load(file)
        start_iter = pickle.load(file)
        bailout_val = pickle.load(file)

        colorizer_name = pickle.load(file)

        colorizer = [col for col in colorizers if col.__name__ == colorizer_name][0]

        drawer_args = {'func'       : func,
                       'max_iters'  : max_iterations,
                       'start_iter' : start_iter,
                       'plane'      : plane,
                       'iter_var'   : iter_var,
                       'num_var'    : num_var,
                       'start_iter' : start_iter,
                       'bailout_val': bailout_val,
                       'screen'     : None,
                       'colorizer'  : colorizer,
                      }

        drawer = fractal_drawer(**drawer_args)
        drawer.zoom_coef = zoom_coef
        drawer.images_stack = stack

        return drawer, (w, h)
