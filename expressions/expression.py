import math
import pdb

class TokenException(Exception):pass

class token:
    op_dict = {
                '+'     : lambda l, r : l + r,
                '-'     : lambda l, r : l - r,
                '*'     : lambda l, r : l * r,
                '/'     : lambda l, r : l / r,
                '**'    : lambda l, r : l ** r,
              }

    func_dict = {
                'sin'   : math.sin,
                'cos'   : math.cos,
                'tan'   : math.tan,
                'asin'  : math.asin,
                'acos'  : math.acos,
                'atan'  : math.atan,
                'cosh'  : math.cosh,
                'sinh'  : math.sinh,
                'tanh'  : math.tanh,
                'exp'  : math.exp,
                }

    handlers = {
                'int'   : lambda string, **kwarsg   : int(string),
                'float' : lambda string, **kwargs   : float(string),
                'op'    : lambda string, **kwargs   : token.op_dict[string](
                                                                            kwargs['left'],
                                                                            kwargs['right']
                                                                            ),
                'func'  : lambda string, **kwargs   : token.func_dict[string](*kwargs['args']),
                'var'   : lambda string, **kwargs   : kwargs['rules'][string]\
                                                                    if len(kwargs) != 0 and
                                                                        kwargs.get('rules') and
                                                                        string in kwargs['rules'] else string,
               }

    def __init__(self, string, token_type):
        if token_type not in token.handlers: raise TokenException('{0} is not a valid token type'.format(token_type))
        self.token_type, self.string = token_type, string

class expression_node(token):
    def __init__(self, string, token_type, left=None, right=None, args=[]):
        token.__init__(self, string, token_type)
        self.left, self.right, self.args = left, right, args
        left, right, args = None, None, []

    def get_value(self, rules={}):
        rslt = token.handlers[self.token_type](self.string,
                                               rules=rules,
                                               left=self.left.get_value(rules=rules) if self.left else None,
                                               right=self.right.get_value(rules=rules) if self.right else None,
                                               args = [x.get_value(rules=rules) for x in self.args] if hasattr(self, 'args') else None
                                              )
        rules = {}
        return rslt

    def __repr__(self):
        return "<expression node '{0}' of type {1}>".format(self.string, self.token_type)
