from expression import token, expression_node, TokenException
import unittest
import math

class TestToken(unittest.TestCase):
    def test_init(self):
        tkn = token('a', 'func')
        tkn = token('3', 'int')

        with self.assertRaises(TokenException):
            tkn = token('baba', 'dqdo')

class TestExpressionNode(unittest.TestCase):
    def test_numbers_and_vars(self):
        node1 = expression_node('5', 'int')
        node2 = expression_node('3.5', 'float')
        node3 = expression_node('a', 'var')

        self.assertEqual(5,     node1.get_value())
        self.assertEqual(3.5,   node2.get_value())
        self.assertEqual('a',   node3.get_value())
        self.assertEqual(12,    node3.get_value(rules={'a':12}))

    def test_op(self):
        plus_node = expression_node('+', 'op',
                                    expression_node('5',   'int'),
                                    expression_node('5.7', 'float'))
        self.assertEqual(10.7, plus_node.get_value())

    def test_func(self):
        sine = expression_node('sin', 'func', args=[expression_node('3.141592', 'float')])
        self.assertEqual(math.sin(3.141592), sine.get_value())

    def test_something_bigger(self):
        cosine_a = expression_node('cos', 'func', args = [expression_node('+', 'op',
                                                                          left = expression_node('exp', 'func',
                                                                                                 args=[expression_node('b', 'var')]),
                                                                          right = expression_node('a', 'var'))])

        self.assertEqual(math.cos(math.exp(1.045)+0.25), cosine_a.get_value({'a':0.25, 'b':1.045}))

if __name__ == '__main__':
	unittest.main()
