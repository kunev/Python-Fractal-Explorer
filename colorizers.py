from pygame import Color

def bw_colorizer(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters\
                    else (255*iterations//max_iters, 255*iterations//max_iters, 255*iterations//max_iters)

def wb_colorizer(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters\
                            else (255-255*iterations//max_iters, 255-255*iterations//max_iters, 255-255*iterations//max_iters)

def green_haze(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters else ((255*iterations//max_iters)//5, 255*iterations//max_iters, (255*iterations//max_iters)//5)

def red_haze(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters else (int(255*iterations/max_iters), int(255*iterations/max_iters)//10, int(255*iterations/max_iters)//10)

def blue_haze(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters else (int(255*iterations/max_iters)//10, int(255*iterations/max_iters)//10, int(255*iterations/max_iters))

def binary_decompozition(iterations, final_val, max_iters):
    return (0,0,0) if iterations == max_iters else (0,0,0) if final_val.imag > 0 else (255,255,255)

def hsva_coloring_continuous(iterations, final_val, max_iters):
    if iterations == max_iters: return (0, 0, 0)

    rslt = Color(0)
    rslt.hsva = (int(360*iterations/max_iters), 100, 100, 1)
    return rslt

def hsva_rev(iterations, final_val, max_iters):
    if iterations == max_iters: return (0,0,0)

    rslt = Color(0)
    rslt.hsva = (int(360-360*iterations/max_iters), 100, 100, 1)

    return rslt

colorizers = [bw_colorizer, wb_colorizer, green_haze, red_haze, blue_haze,
binary_decompozition, hsva_coloring_continuous, hsva_rev]
