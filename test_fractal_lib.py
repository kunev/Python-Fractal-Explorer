import unittest
import random, time
from complex_iteration import *
from evaluation import *
from tokens import *

from math import sin, cos

rand = random.random
random.seed(time.time())

class pseudo_surface:
    def __init__(self, w, h):
        self.w, self.h = w, h

    def get_width(self):
        return self.w

    def get_height(self):
        return self.h


class TestGeometry(unittest.TestCase):
    def setUp(self):
        self.surface = pseudo_surface(1280,800)
        self.pln = complex_plane(-8+5j,8-5j)

    def random_pixel_coords(self):
        return (round(rand() * self.surface.get_width()), round(rand() * self.surface.get_height()))

    def random_complex(self):
        return self.pln.upper_left + complex((rand()*self.pln.width)-(rand()*self.pln.height))

    def test_pixel_to_complex(self):
        z1 = self.pln.pixel_to_complex(self.surface, (0,0))
        z2 = self.pln.pixel_to_complex(self.surface, (1280, 0))
        z3 = self.pln.pixel_to_complex(self.surface, (1280, 800))
        z4 = self.pln.pixel_to_complex(self.surface, (0, 800))

        self.assertAlmostEqual(z1, self.pln.upper_left)
        self.assertAlmostEqual(z2, self.pln.upper_left + self.pln.width)
        self.assertAlmostEqual(z3, self.pln.lower_right)
        self.assertAlmostEqual(z4, self.pln.lower_right - self.pln.width)

    def test_complex_to_pixel(self):
        pxl1 = self.pln.complex_to_pixel(self.surface, -8+5j)
        pxl2 = self.pln.complex_to_pixel(self.surface, 8+5j)
        pxl3 = self.pln.complex_to_pixel(self.surface, 8-5j)
        pxl4 = self.pln.complex_to_pixel(self.surface, -8-5j)

        self.assertEqual((0.0,0.0), pxl1)
        self.assertEqual((1280.0,0.0), pxl2)
        self.assertEqual((1280.0, 800.0), pxl3)
        self.assertEqual((0.0, 800.0), pxl4)

    def test_uniquity_of_conversions(self):
        some_random_pixels = [self.random_pixel_coords() for _ in range(10)]
        some_random_numbers = [self.random_complex() for _ in range(10)]

        respective_numbers = [self.pln.pixel_to_complex(self.surface, pixel) for pixel in some_random_pixels]
        respective_pixels = [self.pln.complex_to_pixel(self.surface, number) for number in some_random_numbers]

        for pixel, number in zip(some_random_pixels, respective_numbers):
            self.assertEqual(self.pln.complex_to_pixel(self.surface, number), pixel)

        for number, pixel in zip(some_random_numbers, respective_pixels):
            delta = abs(self.pln.pixel_to_complex(self.surface, pixel) - number)
            self.assertGreater(0.01, delta)
        

class TestTokens(unittest.TestCase):
    def test_token_types(self):
        left_par = token('(')
        var1, var2, var3 = token('z'), token('a'), token('c')
        op1, op2 = token('+'), token('**')

        self.assertEqual(token.PAREN, left_par.type)
        self.assertEqual(token.VAR, var1.type)
        self.assertEqual(token.VAR, var2.type)
        self.assertEqual(token.VAR, var3.type)
        self.assertEqual(token.OP, op1.type)
        self.assertEqual(token.OP, op2.type)

    def test_raise_bad_tokens(self):
        with self.assertRaises(BadTokenValue):
            token('baba')
        with self.assertRaises(BadTokenValue):
            token('')
        with self.assertRaises(BadTokenValue):
            token('++')
        with self.assertRaises(BadTokenValue):
            token('***')
        with self.assertRaises(BadTokenValue):
            token('abc')

    def test_raise_on_type_change(self):
        with self.assertRaises(TokenException):
            t = token('a')
            t.type = 'naughty_token'

class TestExpressions(unittest.TestCase):
    def test_expression_creation(self):
        expr = expression('12*3-c/z**2/(12.3-6j*z)')

        expected_tokens = ['12', '*', '3', '-', 'c', '/', 'z', '**', '2', '/',
'(', '12.3', '-', '6j', '*', 'z', ')']

        self.assertEqual(expected_tokens, expr.tokens)

    def test_postfix_notation(self):
        expr = expression('5*4-3/2')
        expr.generate_postfix()
        postfix_string = ' '.join([tkn.str for tkn in expr.postfix])
        self.assertEqual(postfix_string, '5 4 * 3 2 / -')

        expr = expression('12**3/(5-2*z)')
        expr.generate_postfix()
        postfix_string = ' '.join([tkn.str for tkn in expr.postfix])
        self.assertEqual(postfix_string, '12 3 ** 5 2 z * - /')

    def test_simple_expression_evaluation(self):
        func = expression('3+15/5*2').get_callable()
        self.assertEqual(func(), 9)

    def test_expression_evaluation_with_arguments(self):
        func = expression('c**2-2').get_callable()
        self.assertEqual(func(c = 7), 47)

        func = expression('z**2-c').get_callable()
        self.assertEqual(func(z = 5, c = 10), 15)

    def test_raise_on_insufficient_arguments_for_callable(self):
        func1, func2 = expression('z**2-c').get_callable(), expression('15-5*g').get_callable()

        with self.assertRaises(ExpressionError):
            func1()
        with self.assertRaises(ExpressionError):
            func1(z = 4)
        with self.assertRaises(ExpressionError):
            func1(c = 7)
        with self.assertRaises(ExpressionError):
            func2()
        with self.assertRaises(ExpressionError):
            func2(t = 15)

    def test_expression_with_functions(self):
        func = expression('2*sqrt(x)').get_callable()
        self.assertEqual(func(x = 25), 10)

    def test_expression_callable_get_var_names(self):
        func = expression('2*z-13/c*r').get_callable()
        self.assertEqual({'z', 'c', 'r'}, func.get_var_names())

        func = expression('12*z-z**2+d*a/4').get_callable()
        self.assertEqual({'a', 'd', 'z'}, func.get_var_names())

    def test_expression_callable_curry(self):
        func = expression('a*b').get_callable()

        curry_func = func.get_curry(a=2)
        self.assertEqual(curry_func(b=3), 6)

        func = expression('12*4-3/6*x+5-z').get_callable()
        curry_func = func.get_curry()

    def test_expression_callable_get_var_names(self):
        func = expression('z+14-2+c/a').get_callable()
        self.assertEqual(func.var_names, {'z', 'c', 'a'})

    def test_expressions_with_something_serious(self):
        func = expression('2*(sin(x) - cos(y/2))/(3+0.032j)**5 + ((5))').get_callable()
        value = 2*(sin(0.034) - cos(13.4323465/2))/(3+0.032j)**5 + ((5))

        self.assertEqual(value, func(x = 0.034, y = 13.4323465))

if __name__ == '__main__':
    unittest.main()
